from flask import Flask, render_template
app = Flask(__name__)
import schedule as Schedules
import schedule_store as sstore
from jinja2 import Template

s1=Schedules.schedule(1,2019,12,1,'mon','12:40')
s2=Schedules.schedule(2,2020,11,1,'tue','4:20')
all_sch=[s1,s2]

@app.route("/")
def main():
	all_sch=sstore.get_all()
	return render_template("jinja_Table.html",all_sch=all_sch)



if name == "main":
	app.run()