from flask import Flask, render_template
app = Flask(__name__)
import schedule as Schedules
from jinja2 import Template

s1=Schedules.schedule(1,2019,12,1,'mon','12:40')
s2=Schedules.schedule(2,2020,11,1,'tue','4:20')
all_sch=[s1,s2]

t=Template("{{str}}")

strTable = "<html><table><tr><th>Schedule_id</th><th>Year</th><th>Month</th><th>Week</th><th>Day_of_week</th><th>Time</th></tr>"
for s in all_sch:
	strRW = "<tr><td>"+str(s.sid)+ "</td><td>"+str(s.year)+"</td>"+"<td>"+str(s.month)+ "</td><td>"+str(s.week)+"</td>"+"<td>"+s.dow+ "</td><td>"+s.time+"</td></tr>"
	strTable = strTable+strRW

@app.route("/all_sch")
def schs():
	return (t.render(str=strTable))

#@app.route('/jinjaman/')
#def jinjaman():
#	try:
#		data = [15, '15', 'Python is good','Python, Java, php, SQL, C++','<p><strong>Hey there!</strong></p>']
#		return render_template("jinjaT.html", data=data)
#	except Exception as e:
#		return(str(e))

@app.route("/")
def main():
	return ("Hello, welcome!")

if __name__ == "__main__":
	app.run()