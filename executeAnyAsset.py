import os
import cv2 
import numpy as np 
from PIL import Image
from playsound import playsound

class executeAnyAsset:

	def execute(self,path):

		filename, file_extension = os.path.splitext(path)

		if file_extension =='.png' : 
			ipath = filename+file_extension
			img  = Image.open(ipath)
			img.show()
			print("Image showed successfully")

		if file_extension =='.jpeg' : 
			ipath = filename+file_extension
			img  = Image.open(ipath)
			img.show()
			print("Image showed successfully") 
		
		if file_extension =='.jpg' : 
			ipath = filename+file_extension
			img  = Image.open(ipath)
			img.show()
			print("Image showed successfully")

		elif file_extension == '.mp3': 
			apath = filename+file_extension
			playsound(apath)
			print("Audio played successfully") 

		elif file_extension == '.mp4': 
			vpath = filename+file_extension
			cap = cv2.VideoCapture(vpath) 
			if (cap.isOpened()== False):
		  		print("Error opening video file") 
			while(cap.isOpened()): 
				ret, frame = cap.read() 
				if ret == True: 
					cv2.imshow('Frame', frame) 
					if cv2.waitKey(25) & 0xFF == ord('q'): 
						break
				else: 
					break
			cap.release() 
			cv2.destroyAllWindows()
			print("Video played successfully") 

		else:
			print('Invalid file type. The file cannot be executed. Please use only .mp3, mp4 or .jpeg/.jpg/.png files.')

