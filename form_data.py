from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def main():
	return render_template('form_schedule.html')

@app.route('/', methods = ['POST'])
def getdata():
	year = request.form['Year']
	month = request.form['Month']
	week = request.form['Week']
	dow = request.form['Day of week']
	date = request.form['Date']
	time = request.form['Time']
	return (year)

if __name__ == "__main__":
	app.run()